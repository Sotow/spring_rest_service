Launch of the project: 
cmd command:
mvn clean
mvn install
java -jar spring_REST_service\out\artifacts\spring_REST_service_jar\spring_REST_service.jar

Go http://localhost:8084/message

Open console and command:
Add user:
fetch(
  '/message', 
  { 
    method: 'POST', 
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({id: (number id), user: '{surname=\'(surname)\', name=\'(name)\', dateOfBirth=\'(dateOfBirth)\', email=\'(email)\', password=\'(password)\'}'})
  }
).then(result => result.json().then(console.log))

Example:
fetch(
  '/message', 
  { 
    method: 'POST', 
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({id: 10, user: '{surname=\'surname\', name=\'name\', dateOfBirth=\'dateOfBirth\', email=\'email\', password=\'password\'}'})
  }
).then(result => result.json().then(console.log))
and F5


Delete user:
fetch('/message/(number deleting messege)', { method: 'DELETE' }).then(result => console.log(result))
Example: 
fetch('/message/0', { method: 'DELETE' }).then(result => console.log(result)) and F5


Search user by ID:
fetch('/message/(number user)').then(response => response.json().then(console.log))
Example: 
fetch('/message/2').then(response => response.json().then(console.log))
or
go to url and append /(number user)
Example:
/0