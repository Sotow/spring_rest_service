package com.service_rest.service_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class ServiceRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceRestApplication.class, args);
	}
}
